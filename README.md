# EXAMEN

Voici venu le jour de l'Examen

## QCM

**1. Le DAO est-il : (plusieurs réponses possibles)**


   - [ ] Un Design Pattern Objet ?
   - [ ] Un Pattern d'Architecture Logicielle ?
   - [ ] Un pattern d'architecture technique ?


**2. Même question pour MVC, peut-il s'agir : (plusieurs réponses possibles)**


   - [ ] D'un Design Pattern Objet ?
   - [ ] D'un Pattern d'Architecture Logicielle ?
   - [ ] D'un pattern d'architecture technique ?


**3. Comment qualifieriez-vous 3/n-Tier ? (plusieurs réponses possibles)**


   - [ ] En tant qu'architecture technique.
   - [ ] En tant qu'architecture logicielle.
   - [ ] En tant que design pattern.


**4. Quels styles architecturaux avez-vous appliqué dans le cadre de ce cours (3 réponses possibles)**


   - [ ] Architecture en appels et retours
   - [ ] Architecture en couches
   - [ ] Architecture centrée sur les données
   - [ ] Architecture en flot de données
   - [ ] Architecture orientée objets
   - [ ] Architecture orientée agents


**5. Parmi les Design Patterns Objet élémentaires suivants, quel est celui qui garantit la possibilité de n'avoir qu'une seule et même instance pour une classe d'objet donnée ?**


   - [ ] DAO
   - [ ] Singleton
   - [ ] Factory
   - [ ] Delegate
   - [ ] Proxy


**6. Parmi les Design Patterns Objet élémentaires suivants, quel est celui qui permet de s'abstraire d'instancier manuellement un objet, c'est-à-dire sans nécessiter l'appel à un constructeur ?**


   - [ ] DAO
   - [ ] Singleton
   - [ ] Factory
   - [ ] Delegate
   - [ ] Proxy


**7. Parmi les Design Patterns Objet élémentaires suivants, quel est celui auquel on délègue le opérations d'accès aux données ?**


- [ ] DAO
- [ ] Singleton
- [ ] Factory
- [ ] Delegate
- [ ] Proxy


**8. Parmi Design Patterns Structurels avancés suivants, quel est celui qui permet de manipuler un framework ou langage en coordonnant ses objets entre eux et favorisant l'application de patterns élémentaires ?**


- [ ] DAO
- [ ] MVC
- [ ] Publish/Subscribe
- [ ] IOC
- [ ] Object Pool
- [ ] Injection de dépendances


**9. Parmi Design Patterns Structurels avancés suivants, quel est celui qui permet à l'Inversion de Controle (IOC) de fonctionner grâce à une programmation déclarative des interactions entre objets/composants ?**


- [ ] DAO
- [ ] MVC
- [ ] Publish/Subscribe
- [ ] IOC
- [ ] Object Pool
- [ ] Injection de dépendances


**10. Parmi Design Patterns Structurels avancés suivants, lequel est-il destiné à l mise en place d'interfaces graphiques ?**


- [ ] DAO
- [ ] MVC
- [ ] Publish/Subscribe
- [ ] IOC
- [ ] Object Pool
- [ ] Injection de dépendances


## EXERCICES

### Exercice 1: Patterns Singleton / Factory / Delegate

- [ ] Sachant que la classe **MyExamFactory** vous permet d'injecter l'examen en cours dont le type est **MyExam**, développer la classe **MyExamDelegate** qui implémente **MyExamDelegateContract**
- [ ] S'assurer que la classe développée répond bien aux exigences en exécutant le test **fr.cnam.foad.nfa035.exam.Nfa035ExamPatternTest#testDelegateToString**


### Exercice 2: Pattern Wrapper et librairie commons-lang

Nouveau pattern: le Wrapper est simplement un objet POJO contenant en son sein, c'est-à-dire sous forme de variable d'instance, un autre objet.
L'objet contenu doit :

 - pouvoir être accédé ou modifié grace aux accesseurs (méthodes getters/setters)
 - Pouvoir être construit avec en paramètre l'objet contenu


- [ ] Implémenter donc **MyExamSortingWrapper**, le wrapper de l'objet **MyExam**
   - Note: il servira également aux exercices 3 et 4 pour une épreuve de tri sur interface Comparable, mais à ce stade, inutile de l'implémenter.
- [ ] S'assurer que la classe développée répond bien aux exigences en exécutant le test **fr.cnam.foad.nfa035.exam.Nfa035ExamPatternTest#testWrapperPattern**


### Exercice 3: librairie commons-lang et ToStringBuilder

- [ ] Au niveau du Wrapper, surcharger la méthode **toString()** en exploitant la librairie **commons-lang** **(3)** de Apache. 
  - Cette librairie "boite-à-outils" vous permet de disposer de différents objet dits "Builders", et notamment le ToStringBUilder.
  - En vous aidant du Net pour la dépendance Maven ET la javadoc, intégrez **commons-lang** **(3)** dans vos dépendances afin que ce code prévu pour votre méthode **toString()** puisse compiler
  ```java
  return ToStringBuilder.reflectionToString(this);
   ```
- [ ] S'assurer que la classe ainsi modifiée répond bien toujours aux exigences des tests précédents mais aussi à celle du test **fr.cnam.foad.nfa035.exam.Nfa035ExamPatternTest#testWrapperPatternToString**

### Exercice 4: Commons-lang avec le CompareToBuilder et l'API du JDK lui-même 


- [ ] Implémentez l'interface **Comparable** au niveau de la classe **MyExamSortingWrapper**
- [ ] Dès lors vous devez donc surcharger la méthode **compareTo()**, exploitez donc l'objet **CompareToBuilder** 
  - Le contrat à respecter est simplement le tri naturel par **Code UE**, **Région** et **Date** , dans cet ordre
  - Aidez-vous de la Javadoc publique de commons-lang, sur le net
  - Aidez-vous aussi de:
  ```java
    return new CompareToBuilder()
        .???
        .???
        .append(this.exam.getDate().getTime(), o.exam.getDate().getTime())
        .???(); 
  ```
- [ ] S'assurer que la classe ainsi modifiée répond bien toujours aux exigences des tests précédents mais aussi à celle du test **fr.cnam.foad.nfa035.exam.Nfa035ExamPatternTest#testCompareToBuilder**



____

## Annexes: diagrammes-classes



```plantuml
@startuml


  namespace fr.cnam.foad.nfa035.exam {
    namespace model {
      class fr.cnam.foad.nfa035.exam.model.MyExam {
          - codeUe : String
          - date : Date
          - ecole : String
          - intitule : String
          - region : String
          + MyExam()
          + MyExam()
          + equals()
          + getCodeUe()
          + getDate()
          + getEcole()
          + getIntitule()
          + getRegion()
          + setCodeUe()
          + setDate()
          + setEcole()
          + setIntitule()
          + setRegion()
          + toString()
      }
    }
  }


  namespace fr.cnam.foad.nfa035.exam {
    namespace model {
      class fr.cnam.foad.nfa035.exam.model.MyExamSortingWrapper {
          + MyExamSortingWrapper()
          + compareTo()
          + getExam()
          + setExam()
          + toString()
      }
    }
  }


  namespace fr.cnam.foad.nfa035.exam {
    namespace pattern {
      class fr.cnam.foad.nfa035.exam.pattern.MyExamDelegate {
          + delegateToString()
      }
    }
  }


  namespace fr.cnam.foad.nfa035.exam {
    namespace pattern {
      interface fr.cnam.foad.nfa035.exam.pattern.MyExamDelegateContract {
          {abstract} + delegateToString()
      }
    }
  }


  namespace fr.cnam.foad.nfa035.exam {
    namespace pattern {
      class fr.cnam.foad.nfa035.exam.pattern.MyExamFactory {
          + getCurrentExam()
      }
    }
  }



  fr.cnam.foad.nfa035.exam.model.MyExamSortingWrapper .up.|> java.lang.Comparable
  fr.cnam.foad.nfa035.exam.model.MyExamSortingWrapper o-- fr.cnam.foad.nfa035.exam.model.MyExam : exam

  fr.cnam.foad.nfa035.exam.pattern.MyExamDelegate .up.|> fr.cnam.foad.nfa035.exam.pattern.MyExamDelegateContract
  fr.cnam.foad.nfa035.exam.pattern.MyExamDelegate o- fr.cnam.foad.nfa035.exam.model.MyExam : exam



@enduml
```



